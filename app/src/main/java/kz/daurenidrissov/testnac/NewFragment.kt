package kz.daurenidrissov.testnac

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.ui.navigateUp
import kotlinx.android.synthetic.main.fragment_new.*

class NewFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myTextTV.text = arguments?.getString("myString") ?: "Hi!"

        goToMainFromNewBt.setOnClickListener {
            it.findNavController().navigate(NewFragmentDirections.actionNewFragmentToMainFragment())
        }
    }
}