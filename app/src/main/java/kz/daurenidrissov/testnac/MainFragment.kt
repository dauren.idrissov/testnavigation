package kz.daurenidrissov.testnac

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_new.*

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goToNewFromMainBt.setOnClickListener {
            val myText = myTextET.text.toString()
            val action = MainFragmentDirections.actionMainFragmentToNewFragment(myText)
            it.findNavController().navigate(action)
        }

        goToAnotherFromMainBt.setOnClickListener {
            it.findNavController().navigate(MainFragmentDirections.actionMainFragmentToAnotherFragment())
        }

    }
}